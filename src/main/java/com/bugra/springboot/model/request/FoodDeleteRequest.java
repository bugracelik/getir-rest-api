package com.bugra.springboot.model.request;

public class FoodDeleteRequest {

    private String name;
    private Integer id;

    public FoodDeleteRequest(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public FoodDeleteRequest(Integer id) {
        this.id = id;
    }

    public FoodDeleteRequest(String name) {
        this.name = name;
    }

    public FoodDeleteRequest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FoodDeleteRequest{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
