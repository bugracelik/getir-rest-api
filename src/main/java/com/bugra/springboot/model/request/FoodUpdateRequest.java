package com.bugra.springboot.model.request;

import org.springframework.data.relational.core.sql.In;

public class FoodUpdateRequest {


    private String isim;
    private String sonKullanmaTarihi;
    private String üretimTarihi;
    private String üretici;
    private Integer stock;
    private Integer id;

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSonKullanmaTarihi() {
        return sonKullanmaTarihi;
    }

    public void setSonKullanmaTarihi(String sonKullanmaTarihi) {
        this.sonKullanmaTarihi = sonKullanmaTarihi;
    }

    public String getÜretimTarihi() {
        return üretimTarihi;
    }

    public void setÜretimTarihi(String üretimTarihi) {
        this.üretimTarihi = üretimTarihi;
    }

    public String getÜretici() {
        return üretici;
    }

    public void setÜretici(String üretici) {
        this.üretici = üretici;
    }


}
