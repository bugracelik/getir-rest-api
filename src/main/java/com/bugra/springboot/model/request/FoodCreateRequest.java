package com.bugra.springboot.model.request;

public class FoodCreateRequest { // string json to model
    private String isim;
    private String sonKullanmaTarihi;
    private String üretimTarihi;
    private String üretici;
    private Integer stock;

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public String getSonKullanmaTarihi() {
        return sonKullanmaTarihi;
    }

    public void setSonKullanmaTarihi(String sonKullanmaTarihi) {
        this.sonKullanmaTarihi = sonKullanmaTarihi;
    }

    public String getÜretimTarihi() {
        return üretimTarihi;
    }

    public void setÜretimTarihi(String üretimTarihi) {
        this.üretimTarihi = üretimTarihi;
    }

    public String getÜretici() {
        return üretici;
    }

    public void setÜretici(String üretici) {
        this.üretici = üretici;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "FoodCreateRequest{" +
                "isim='" + isim + '\'' +
                ", sonKullanmaTarihi='" + sonKullanmaTarihi + '\'' +
                ", üretimTarihi='" + üretimTarihi + '\'' +
                ", üretici='" + üretici + '\'' +
                ", stock=" + stock +
                '}';
    }
}
