package com.bugra.springboot.model;

import org.springframework.data.relational.core.sql.In;

public class FoodDto {
    private String isim;
    private String üretici;
    private Integer stock;

    @Override
    public String toString() {
        return "FoodDto{" +
                "isim='" + isim + '\'' +
                ", üretici='" + üretici + '\'' +
                ", stock=" + stock +
                '}';
    }

    public FoodDto() {
    }

    public FoodDto(String isim, String üretici, Integer stock) {
        this.isim = isim;
        this.üretici = üretici;
        this.stock = stock;
    }

    public FoodDto(String isim, String üretici) {
        this.isim = isim;
        this.üretici = üretici;
    }

    public FoodDto(String isim, Integer stock) {
        this.isim = isim;
        this.stock = stock;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public String getÜretici() {
        return üretici;
    }

    public void setÜretici(String üretici) {
        this.üretici = üretici;
    }
}

