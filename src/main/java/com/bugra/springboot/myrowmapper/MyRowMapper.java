package com.bugra.springboot.myrowmapper;

import com.bugra.springboot.model.FoodDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MyRowMapper implements RowMapper<FoodDto> {

    @Override
    public FoodDto mapRow(ResultSet resultSet, int i) throws SQLException {

        Integer id = resultSet.getInt("id");
        String isim = resultSet.getString("isim");
        String sonKullanmaTarihi = resultSet.getString("sonKullanmaTarihi");
        String üretimTarihi = resultSet.getString("üretimTarihi");
        String üretici = resultSet.getString("üretici");
        Integer stock = resultSet.getInt("stock");

        FoodDto foodDto = new FoodDto(isim, üretici, stock);
        return foodDto;
    }
}

