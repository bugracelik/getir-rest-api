package com.bugra.springboot.myrowmapper;

import com.bugra.springboot.model.FoodDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RowMapperForStockInformation implements RowMapper<FoodDto> {

    @Override
    public FoodDto mapRow(ResultSet resultSet, int i) throws SQLException {

        String isim = resultSet.getString("isim");

        Integer stock = resultSet.getInt("stock");

        FoodDto foodDto = new FoodDto(isim, stock);
        return foodDto;
    }
}
