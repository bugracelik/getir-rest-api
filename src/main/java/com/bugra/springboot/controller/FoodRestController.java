package com.bugra.springboot.controller;

import com.bugra.springboot.domain.Food;
import com.bugra.springboot.model.FoodDto;
import com.bugra.springboot.model.request.FoodCreateRequest;
import com.bugra.springboot.model.request.FoodDeleteRequest;
import com.bugra.springboot.model.request.FoodUpdateRequest;
import com.bugra.springboot.services.FoodService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

// rest api over http server tomcat servlet, tüm firmalar reste uygun api yazar.
// restin fikirlerine uygun api http server, client istek atıcak, response food, product, category, customer, supplier ( resources)

// resource yaratılacak post ile gelincek
// resource silincekse delete ile

@Component
@RestController
@RequestMapping("food/")// spring
public class FoodRestController {

    @Autowired
    FoodService foodService;

    // get ile gelmek data almak read concern
    @GetMapping("all")
    public List<FoodDto> getAllFood() throws IOException {
        return foodService.getAllFood();
    }

    @GetMapping("{foodname}")
    public List<FoodDto> getAllFoodByName(@PathVariable String foodname) {
        return foodService.getAllFoodByName(foodname);
    }

    @GetMapping("{foodId}")
    public List<FoodDto> getFoodById(@PathVariable Integer foodId){
        return foodService.getFoodById(foodId);
    }

    // post ile gelmek server tarafta yaratmak ( write mapping)
    @PostMapping("save")
    public void addFood(HttpServletRequest request) throws IOException { // header string body string
        String jsonString = new String(request.getInputStream().readAllBytes()); // body string
        ObjectMapper objectMapper = new ObjectMapper();
        Food food = objectMapper.readValue(jsonString, Food.class);// deseri
        foodService.saveFood(food);
    }

    // post ile gelmek server tarafta yaratmak ( write mapping)
    @PostMapping("save/spring")
    public void addFood(@RequestBody FoodCreateRequest request) throws IOException {
        foodService.addFood(request);
    }


    @PutMapping("{id}")
    public void updateFoodById(@RequestBody FoodUpdateRequest foodUpdateRequest, @PathVariable Integer id) {
        foodService.updateFoodById(foodUpdateRequest);
    }

    @DeleteMapping("{id}")
    public void deleteFoodById(@PathVariable Integer id){
        foodService.deleteFoodById(id);
    }


    @DeleteMapping("{id}/{name}")
    public void deleteFoodByIdAndNameWithUrl(@PathVariable Integer id, @PathVariable String name) {
        foodService.deleteFoodByIdAndNameWithUrl(id, name);
    }


    @DeleteMapping("delete")
    public void deleteFood(@RequestBody FoodDeleteRequest foodDeleteRequest) {
        foodService.deleteFood(foodDeleteRequest);
    }




}

