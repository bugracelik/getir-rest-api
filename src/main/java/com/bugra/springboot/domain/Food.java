package com.bugra.springboot.domain;


public class Food {

    private Integer id;
    private String isim;
    private String sonKullanmaTarihi;
    private String üretimTarihi;
    private String üretici;
    private Integer stock;

    public Food(Integer id, String isim, String sonKullanmaTarihi, String üretimTarihi, String üretici, Integer stock) {
        this.id = id;
        this.isim = isim;
        this.sonKullanmaTarihi = sonKullanmaTarihi;
        this.üretimTarihi = üretimTarihi;
        this.üretici = üretici;
        this.stock = stock;
    }

    public Food() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public String getSonKullanmaTarihi() {
        return sonKullanmaTarihi;
    }

    public void setSonKullanmaTarihi(String sonKullanmaTarihi) {
        this.sonKullanmaTarihi = sonKullanmaTarihi;
    }

    public String getÜretimTarihi() {
        return üretimTarihi;
    }

    public void setÜretimTarihi(String üretimTarihi) {
        this.üretimTarihi = üretimTarihi;
    }

    @Override
    public String toString() {
        return "Food{" +
                "id=" + id +
                ", isim='" + isim + '\'' +
                ", sonKullanmaTarihi='" + sonKullanmaTarihi + '\'' +
                ", üretimTarihi='" + üretimTarihi + '\'' +
                ", üretici='" + üretici + '\'' +
                ", stock=" + stock +
                '}';
    }

    public String getÜretici() {
        return üretici;
    }

    public void setÜretici(String üretici) {
        this.üretici = üretici;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
}
