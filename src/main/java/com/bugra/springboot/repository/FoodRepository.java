package com.bugra.springboot.repository;

import com.bugra.springboot.domain.Food;
import com.bugra.springboot.model.FoodDto;
import com.bugra.springboot.model.request.FoodCreateRequest;
import com.bugra.springboot.model.request.FoodDeleteRequest;
import com.bugra.springboot.model.request.FoodUpdateRequest;
import com.bugra.springboot.myrowmapper.MyRowMapper;
import com.bugra.springboot.myrowmapper.RowMapperForStockInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.List;

@Component
public class FoodRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void addFood(String isim, String sonKullanmaTarihi, String üretimTarihi, String üretici) throws SQLException {
        String sqlword = String.format("INSERT INTO food (isim, sonKullanmaTarihi, üretimTarihi, üretici) VALUES ( '%s', '%s', '%s', '%s')", isim, sonKullanmaTarihi, üretimTarihi, üretici);
        jdbcTemplate.execute(sqlword);
    }

    public List<FoodDto> getAllFood() {
        List<FoodDto> foodDtoList = jdbcTemplate.query("select * from food ", new MyRowMapper());
        return foodDtoList;
    }

    public List<FoodDto> getAllFoodByName(String foodname) {
        //select * from food where isim = 'kahve';
        String sqlWord = String.format("select *  from food where isim = '%s'", foodname);
        List<FoodDto> foodDtoList = jdbcTemplate.query(sqlWord, new RowMapperForStockInformation());
        return foodDtoList;
    }

    public void updateFoodById(FoodUpdateRequest foodUpdateRequest) {
        String sqlWord = String.format("update food set üretimTarihi = '%s', sonKullanmaTarihi = '%s', isim = '%s', üretici = '%s', stock = %d WHERE id = %d",
                foodUpdateRequest.getÜretimTarihi(),
                foodUpdateRequest.getSonKullanmaTarihi(),
                foodUpdateRequest.getIsim(),
                foodUpdateRequest.getÜretici(),
                foodUpdateRequest.getStock(),
                foodUpdateRequest.getId());
        jdbcTemplate.execute(sqlWord);
    }

    public void addFood(FoodCreateRequest request) {
        //yy-mm-dd

        String sqlword = String.format("INSERT INTO food (isim, sonKullanmaTarihi, üretimTarihi, üretici, stock) VALUES ( '%s', '%s', '%s', '%s', %d)",
                request.getIsim(),
                request.getSonKullanmaTarihi(),
                request.getÜretimTarihi(),
                request.getÜretici(),
                request.getStock());
        jdbcTemplate.execute(sqlword);
    }

    public void deleteFood(FoodDeleteRequest foodDeleteRequest) {
        String sqlWord = String.format("DELETE FROM food WHERE id = %d and isim = '%s';", foodDeleteRequest.getId(), foodDeleteRequest.getName());
        jdbcTemplate.execute(sqlWord);
    }

    public void deleteFoodByIdAndNameWithUrl(Integer id, String name) {
        String sqlWord = String.format("delete from food where id = %d and isim = '%s';", id, name);
        jdbcTemplate.execute(sqlWord);
    }

    public void saveFood(Food food) {
        String sqlWord = String.format("ınsert into food (isim, sonKullanmaTarihi, üretimTarihi, üretici, stock) values ('%s', '%s', '%s', '%s', %d)",
                food.getIsim(), food.getSonKullanmaTarihi(), food.getÜretimTarihi(), food.getÜretici(), food.getStock());
        jdbcTemplate.execute(sqlWord);
    }

    public void deleteFoodById(Integer id) {
        String sqlWord = String.format("delete from food where id = %d ", id);
        jdbcTemplate.execute(sqlWord);
    }

    public void updateFood(Integer id) {
        //...
    }

    public List<FoodDto> getFoodById(Integer id) {
        String sqlWord = String.format("select * from food where id = %d", id);
        return jdbcTemplate.query(sqlWord, new MyRowMapper());
    }

}