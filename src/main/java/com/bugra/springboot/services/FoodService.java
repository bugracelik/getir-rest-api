package com.bugra.springboot.services;

import com.bugra.springboot.domain.Food;
import com.bugra.springboot.model.FoodDto;
import com.bugra.springboot.model.request.FoodCreateRequest;
import com.bugra.springboot.model.request.FoodDeleteRequest;
import com.bugra.springboot.model.request.FoodUpdateRequest;
import com.bugra.springboot.repository.FoodRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;

@Component
@Slf4j
public class FoodService {

    @Autowired
    FoodRepository foodRepository;

    public void addFood(String isim, String sonKullanmaTarihi, String üretimTarihi, String üretici) throws SQLException {
        try {
            validForDate(sonKullanmaTarihi, üretimTarihi);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            return;
        }
        foodRepository.addFood(isim, sonKullanmaTarihi, üretimTarihi, üretici);
    }

    private void validForDate(String sonKullanmaTarihi, String üretimTarihi) {
        try {
            //kullanıcının tarihi 2020-12-28 formatına uygun olursa parse edilir...
            //yyyy-MM-dd (ISO-8601 calendar system)
            LocalDate parsedLocalSonKullanmaTarihi = LocalDate.parse(sonKullanmaTarihi);
            LocalDate parse = LocalDate.parse(üretimTarihi);
            //parse işlemi gerçekleşiyorsa exception fırlatmıyor demektir.
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public List<FoodDto> getAllFood() {
        return foodRepository.getAllFood();
    }

    public List<FoodDto> getAllFoodByName(String foodname) {
        return foodRepository.getAllFoodByName(foodname);
    }

    public void updateFoodById(FoodUpdateRequest foodUpdateRequest) {
        foodRepository.updateFoodById(foodUpdateRequest);
    }

    public void deleteFood(FoodDeleteRequest foodDeleteRequest) {
        foodRepository.deleteFood(foodDeleteRequest);
    }

    public void deleteFoodByIdAndNameWithUrl(Integer id, String name) {
        foodRepository.deleteFoodByIdAndNameWithUrl(id, name);
    }

    public void addFood(FoodCreateRequest request) {
        foodRepository.addFood(request);
        //log.info("service layer eklendi {}", request);
    }

    public void saveFood(Food food) {
        foodRepository.saveFood(food);
    }

    public void deleteFoodById(Integer id) {
        foodRepository.deleteFoodById(id);
    }

    public void updateFood(Integer id) {
        foodRepository.updateFood(id);
    }

    public List<FoodDto> getFoodById(Integer id) {
       return foodRepository.getFoodById(id);
    }
}
