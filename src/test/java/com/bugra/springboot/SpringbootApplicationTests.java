package com.bugra.springboot;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static java.time.LocalDate.parse;

class SpringbootApplicationTests {

    @Test
    void local_date_parse() {
        String correctDate = "2020-12-28";
        String wrongDate = "12-09-2020";
        LocalDate date = LocalDate.of(2020, 3, 28);
        LocalDate parse = parse(correctDate);

        LocalDate now = LocalDate.now();
        LocalDate localDate = now.minusDays(20);
        System.out.println(now);
        System.out.println(localDate);
        LocalDate localDate1 = now.plusDays(20);
        System.out.println(localDate1);


        boolean leapYear = date.isLeapYear();
        System.out.println(leapYear);


        //assertDoesNotThrow(() -> LocalDate.parse(correctDate));

    }

    @Test
    void string_test(){
       String my_string = new String("merhaba");
        System.out.println(my_string);
        char [] chars = {'a', 'b', 'b', 'a'};
        String s = new String(chars);
        System.out.println(s);
        my_string = "babam";
        System.out.println(my_string);
        byte[] bytes = my_string.getBytes();
        for (byte b : bytes){
            System.out.println(b);
        }
        System.out.println("**********");
        String baba = "baba";
        String baBa = "BaBa";

        int i = baba.compareTo(baBa);
        System.out.println(i);

    }

    @Test
    void work_space(){
        char ch = 40000;
        System.out.println(ch);
    }





}